(function($) {
	function loadImage(url, callback) {
		var image = new Image();
		image.onload = function() {
			callback(image);
		}
		image.src = url;
	}

	$(document).ready(function() {
		$('img[data-hq]').each(function() {
			var img = this;
			var attributes = $(img).prop("attributes");
      
			loadImage($(img).data('hq'), function(hqImage) {
		        $.each(attributes, function() {
					if (this.name != 'src')
		          		$(hqImage).attr(this.name, this.value);
		        });
				$(img).replaceWith(hqImage);
			});
		});
	});
})(jQuery);
